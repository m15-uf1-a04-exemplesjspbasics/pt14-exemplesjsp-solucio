<%@page import="index.Validation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP DEMOS - PT4-EX1- CÀLCUL ÀREES.</title>
        <!-- Boostrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>JSP DEMOS - PT4-EX1-CÀLCUL ÀREA D'UN RECTANGLE</h1>
        <header>
            <%@include file="header.jsp" %>
        </header>
        <main>
            <h3>M15-UF1-PT4-JSP-EX1-CÀLCUL ADN</h3>
            <form method="post" action="arees.jsp">
                <label for="base">Base (en cms):</label>
                <input type="text" name="base" id="base" placeholder="10"><br/>
                <label for="alsada">Alçada (en cms)</label>
                <input type="text" name="alsada" id="alsada" placeholder="10"><br/>
                <input type="submit" name="ok" value="Enviar"/>
            </form>  

            <%
                //si ha clicat o no al botó del formulari
               if(request.getParameter("ok")!=null) {

                  double base, alsada;
                  //debería validar los valores de mis cajas
                  //según lo entrado, me debería salir o no un resultado
                  //crear una clase de validacion

                  base=Validation.validaDouble(request.getParameter("base"));
                  alsada=Validation.validaDouble(request.getParameter("alsada"));

                  if(base <0 || alsada <0){
                      out.println("Has d'introduir números positius");
                  }else{
                    CalculaArees indice = new CalculaArees(base,alsada);

                    out.println("L'àrea és de: <br/>");
                    out.println(String.format("%.2f", indice.calculaArea()));
                  }
               }

            %>
        </main>
        <footer>
            <%@include file="footer.jsp" %>
        </footer>
    </body>
</html>
