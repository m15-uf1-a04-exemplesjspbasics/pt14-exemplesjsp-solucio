<%@page import="index.ADN_Manager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style rel="stylesheet">
            body {background-color: powderblue;}
            h2   {color: red;}
            p    {color: green;}
        </style>
        <!-- Boostrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>GENERADOR ADN</title>
    </head>
    <body>
        <header>
            <%@include file="header.jsp" %>
        </header>
        <main>
            <h3>M15-UF1-PT4-JSP-> EX4 - GENERACIÓ CADENES ADN</h3>
            <div class="container">
            <%
               // Si ha clicat o no al botó del formulari
               if(request.getParameter("ok")!=null) {
                  // Obtenció ADN del form.
                  String num_trossos = request.getParameter("num_trossos");
                  int nTrossos = 0;
                  int lTrossos = 0;
                  if(num_trossos!=null) {
                    nTrossos = Integer.parseInt(num_trossos);
                  }
                  String long_trossos = request.getParameter("long_trossos");
                  if(long_trossos!=null) {
                    lTrossos = Integer.parseInt(long_trossos);
                  }
                  
                  ADN_Manager adnManager = new ADN_Manager();
                  String adn = "";
                  if(lTrossos > 0 && nTrossos < 60 && 
                     nTrossos > 0 && nTrossos < 60) {
                     // Pintarem un grid amb fileres de 4,3,2 o 1.
                     // https://getbootstrap.com/docs/4.0/layout/grid/
                     for (int i = 0; i < nTrossos; i++) {
                        // Si es múltiple de 4 obrim el div de la filera.
                        if(i%4==0) {
                            out.println("<div class='row'>");
                        }
                        adn = adnManager.cadenaADNAleatoria(lTrossos);
                        out.println(
                                "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12'>"
                                    +adn+
                                "</div>");
                        // Si el número abans múltiple de 4 tanquem el div de la filera.
                        if(i%4==3) {
                            out.println("</div>");    
                        }
                     }
                  }
                  out.println("</div>");
                  out.println("<p>Núm. trossos ADN generats = " + num_trossos + "</p>");
                  out.println("<p>Long. trossos ADN generats = " + long_trossos + "</p>");
                }
              %>
            </div>
        </main>
        <footer>
            <%@include file="footer.jsp" %>
        </footer>
    </body>
</html>
