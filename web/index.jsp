<%-- 
    Document   : VALIDACIÓ FORMULARI COMENTARIS
    Author     : DAWBIO - PROFE
--%>
<%@page import="ValidaFormComments.ValidadorFormularis"%>
<%@page import="ValidaFormComments.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- <style rel="stylesheet" >
            body {background-color: powderblue;}
            h2   {color: red;}
            p    {color: green;}
        </style> -->
        <link rel="stylesheet" href="./css/styles.css"> 
        <!-- Boostrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>M15-UF1-PT4-JSP-EX3-FORMPAG1</title>
    </head>
    <body>
        <header>
            <%@include file="header.jsp" %>
        </header>
        <main>
            <h3>M15-UF1-PT4-JSP-EX3-FORM DE COMENTARIS</h3>
            <%
           boolean campsInformats = false;
           boolean campsValids = false;
           
           if(request.getParameter("btnEnviar")!=null) {
              // 1. VALIDACIO CAMPS NO BUITS.
             String nom = request.getParameter("nom");
             String comment = request.getParameter("comment");
             String spamok = request.getParameter("spamok");
             String email = request.getParameter("email");
           
              ValidadorFormularis validador = new ValidadorFormularis(); 
              campsInformats = 
                 validador.validaCampNoBuit(nom) && 
                     validador.validaCampNoBuit(comment) &&
                        validador.validaCampNoBuit(email);
              
              campsInformats = campsInformats && spamok!=null
                      && spamok.equals("true");
              // out.println("campsInformats="+campsInformats);
              
              if (campsInformats) {
                  boolean validComment = 
                          validador.validaNoParaulesOfensives(comment);
                  boolean validEmail = validador.validaCampEmail(email);
                  
                  // 2. VALIDACIO CAMPS CORRECTES.
                  campsValids = validComment && validEmail;
                      
                  out.println(campsValids);
                  // Si els camps son valids anem a la altra JSP
                  if(campsValids) {
                    request.getRequestDispatcher("formvalid.jsp").forward(request, response);
                    
                  } else {
                      // Mostrar camps que no son valids i el motiu.
                       out.println("<strong style=\"color:red;\">Els següents camps són incorrectes: Email o Comentari.</strong>");
                  }
                  
              } else {
                // Si no es vàlid, retorna missatge.
                out.println("<strong style=\"color:red;\">T'ha faltat omplir camps. Alguns estaven buits.</strong>");
              }
              // out.println("el comentari no és vàlid");
              
           } else {
                // Si no es vàlid, retorna missatge.
                // out.println("Error al enviar el formulari.");
           }
           
        %>
            <div class="container">
                <form method="post" action="">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nom">Nom (*)</label>
                             <input id="nom" class="form-control" type="text" name="nom" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email (*)</label>
                            <input id="email" class="form-control" type="text" name="email" />
                        </div>

                        <div class="form-group col-md-12">
                            <label for="comment">Comentari (*)</label>
                    <!--         <select name="conversio">
                                    <option value="adn-arn">ADN-ARN</option>
                                    <option value="arn-adn">ARN-ADN</option>
                                 </select>-->
                            <textarea id="comment" name="comment" rows="10" 
                                minlength="10" class="form-control" maxlength="140"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                            <input type="checkbox" class="btn btn-primary" name="spamok" value="true"/>(*) Acepto recibir spam
                            <input type="submit" class="btn btn-primary" name="btnEnviar" value="Enviar"/>
                    </div>
                </form>
            </div>
        
        </main>
        <footer>
            <%@include file="footer.jsp" %>
        </footer>
    </body>
</html>
