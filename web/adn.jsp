<%@page import="index.ADN_Manager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style rel="stylesheet">
            body {background-color: powderblue;}
            h2   {color: red;}
            p    {color: green;}
        </style>
        <!-- Boostrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>VALIDACIÓ ADN</title>
    </head>
    <body>
        <header>
            <%@include file="header.jsp" %>
        </header>
        
        <main>
            <h3>M15-UF1-PT4-JSP-EX2 - VALIDACIO ADN</h3>
            <form method="post"> 
    <!--              action="index.jsp"-->
                 <label for="ADN_field">ADN:</label> <br>
                 <textarea id="ADN_field" name="ADN_field" rows="10" cols="30"></textarea>
                 <br>
                <input type="submit" name="ok" value="Calcular"/>
            </form>  
            <%
                // Si ha clicat o no al botó del formulari
               if(request.getParameter("ok")!=null) {
                  // Obtenció ADN del form.
                  String ADN_field = request.getParameter("ADN_field");

                  // Valida l'ADN 
                  ADN_Manager adnManager = new ADN_Manager();
                  boolean result = adnManager.validaADN(ADN_field);

                  // Si es vàlid, retorna resultat.
                  if(result) {
                    // Mostrar resultats.
                    StringBuilder sb = new StringBuilder();
                    sb.append("L'ADN és vàlid!");
                    out.println(sb.toString());

                    // Calculo de las bases.
                    int numAdenines = adnManager.numAdenines(ADN_field);
                    int numCitosines = adnManager.numCitosines(ADN_field);
                    int numGuanines = adnManager.numGuanines(ADN_field);
                    int numTimines = adnManager.numTimines(ADN_field);
                    
                  %>
                  <h2>Número Adenines <%=numAdenines %></h2>
                  <h2>Número Citosines <%=numCitosines %></h2>
                  <h2>Número Guanines <%=numGuanines %></h2>
                  <h2>Número Timines <%=numTimines %></h2>
                  <%
                      } else {
                      // Si no es vàlid, retorna missatge.
                      out.println("L'ADN NO ÉS VALID, NOMÉS POT TENIR A,G,C,T ");
                  }
               }
            %>
        </main>
        <footer>
            <%@include file="footer.jsp" %>
        </footer>
    </body>
</html>
