<nav class="navbar sticky-top navbar-dark bg-warning">
    <h2>Pt14 - Exercicis de funcionament de JSP</h2>
    <nav class="nav nav-pills flex-column flex-sm-row">
        <a class='flex-sm-fill text-sm-center nav-link opt-menu' href='arees.jsp'>Ex1-Arees</a>
        <a class='flex-sm-fill text-sm-center nav-link opt-menu' href='adn.jsp'>Ex2-ADN</a>
        <a class='flex-sm-fill text-sm-center nav-link opt-menu' href='index.jsp'>Ex3-Comentaris</a>
        <a class='flex-sm-fill text-sm-center nav-link opt-menu' href='generadn_form.jsp'>Ex4-Opcional</a>
    </nav>
</nav>
