<%@page import="index.ADN_Manager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style rel="stylesheet">
            body {background-color: powderblue;}
            h2   {color: red;}
            p    {color: green;}
        </style>
        <!-- Boostrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>Validació ADN</title>
    </head>
    <body>
        <header>
            <%@include file="header.jsp" %>
        </header>
        
        <main>
            <h3>M15-UF1-PT4-JSP-> EX4 - GENERACIÓ CADENES ADN</h3>
            <div class="container">
                <form method="post" action="generadn_result.jsp">
                    <div class="form-row">
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="num_trossos">Número trossos</label> 
                            <input id="num_trossos" class="form-control" name="num_trossos" type="number"></input> <br/>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label for="long_trossos">Longitud trossos</label> 
                            <input id="long_trossos" class="form-control" name="long_trossos" type="number"></input><br/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <input type="submit" class="btn btn-primary" name="ok" value="Calcular"/>
                        </div>
                    </div>
                </form>
            </div>
        </main>
        <footer>
            <%@include file="footer.jsp" %>
        </footer>
    </body>
</html>
