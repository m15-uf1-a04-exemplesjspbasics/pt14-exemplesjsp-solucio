<%-- 
    Document   : VALIDACIÓ FORMULARI COMENTARIS
    Author     : DAWBIO - PROFE
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/styles.css">
        <title>M15-UF1-PT4-JSP-EX3-FORMVALID</title>
    </head>
    <body>
        <h2>COMENTARI ENVIAT CORRECTAMENT</h2>
        <h3>Gràcies per participar, <%=request.getParameter("nom")%></h3>
        <p>Rebràs la resposta al teu email, <%=request.getParameter("email")%></p>
        <a href="index.jsp">TORNAR</a>
    </body>
</html>
